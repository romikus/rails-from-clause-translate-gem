$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'from_clause_translate/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'from_clause_translate'
  s.version     = FromClauseTranslate::VERSION
  s.authors     = ['Roman Kushin']
  s.email       = ['romadzao@gmail.com']
  s.homepage    = 'https://gitlab.com/romikus/rails-from-clause-translate-gem'
  s.summary     = 'For database record translations'
  s.description = 'Translates record using SQL from clause.'
  s.license     = 'MIT'

  s.files = Dir['lib/**/*', 'MIT-LICENSE', 'README.md']
  s.require_path = 'lib'

  s.add_dependency 'activerecord'

  s.add_development_dependency 'pg'
  s.add_development_dependency 'rspec'
  s.add_development_dependency 'standalone_migrations'
  s.add_development_dependency 'database_cleaner'
end
