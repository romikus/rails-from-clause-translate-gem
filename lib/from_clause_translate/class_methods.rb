module FromClauseTranslate::ClassMethods
  def translates(*columns)
    options = columns.extract_options!
    _translation_data.add_columns(columns)
    translates_plurals options[:plurals]
  end

  def translates_plurals(plurals)
    plurals && plurals.each do |plural|
      _translation_data.add_plural(plural)
    end
  end

  def translates?(column)
    _translation_data.translates? column
  end

  def translated(*columns)
    scope = current_scope || all
    scope.add_translated_columns columns
    scope
  end

  def _translation_data
    @translation_data ||= FromClauseTranslate::TranslationData.new(self)
  end
end
