class FromClauseTranslate::Fallback
  attr_reader :model, :fallback

  def initialize(model, hash)
    @model = model
    @fallback = hash[:fallback]

    if fallback.nil?
      create_fallback_from_i18n(hash[:name])
    elsif !fallback.is_a? Hash
      @fallback = {_: fallback}
    end
  end

  def wrap(selection, column, locale)
    wrap_with_fallbacks selection, fallback[locale], column, locale
    wrap_with_fallbacks selection, fallback[:_], column, locale
    selection
  end

  private

  def wrap_with_fallbacks(selection, fallbacks, column, locale)
    return unless fallbacks

    if fallbacks.is_a? Array
      fallbacks.each do |fallback|
        wrap_with_fallbacks selection, fallback, column, locale
      end
      return
    end

    fallbacks = "#{model.table_name}.#{fallbacks}" if fallbacks.is_a? Symbol
    return if fallbacks == "#{model.table_name}.#{column}_#{locale}"

    selection.replace "COALESCE(#{selection}, #{fallbacks})"
  end

  def create_fallback_from_i18n(column)
    if I18n.fallbacks.empty?
      # invoke fallbacks generation
      I18n.available_locales.each { |locale| I18n.fallbacks[locale] }
    end
    @fallback = {}
    I18n.fallbacks.each do |locale, array|
      result = []
      fallback[locale] = result
      array.each do |fallback|
        result << "#{column}_#{fallback}" unless fallback == locale
      end
    end
  end
end
