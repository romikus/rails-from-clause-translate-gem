require 'from_clause_translate/translated_columns_builder'

module ActiveRecord::Relation::Extended
  def arel(aliases = nil)
    arel = super
    apply_translates
    arel.instance_variable_get(:@ctx).source.left = @from_clause if @from_clause
    arel
  end

  def add_translated_columns(columns)
    (@values[:translated] ||= []).concat columns
  end

  def add_translated_column(column)
    (@values[:translated] ||= []) << column
  end

  def apply_translates
    return if @translates_applied || !@values[:translated]

    @translates_applied = true

    columns = translated_columns_build
    return unless columns

    rel = unscoped.select(columns)
    rel.instance_variable_set(:@translates_applied, true)
    replace_plurals_in_projections
    @from_clause = Arel::Nodes::SqlLiteral.new(
      "(#{rel.arel.to_sql}) #{@klass.table_name}"
    )
  end

  def translated_columns_build
    FromClauseTranslate::TranslatedColumnsBuilder.new(
      self, @klass, @without_projections
    ).perform
  end

  def perform_calculation(operation, column_name)
    return super operation, column_name if @without_projections

    rel = spawn
    rel.instance_variable_set(:@without_projections, true)
    rel.add_translated_column column_name if column_name
    rel.perform_calculation operation, column_name
  end

  def replace_plurals_in_projections
    @arel.projections.each do |column|
      next unless column.is_a? String

      name = column[0] == '"' ? column[1...-1] : column
      plural = @klass._translation_data.plurals[name.to_sym]
      column.replace plural if plural
    end
  end
end

class ActiveRecord::Relation
  prepend ActiveRecord::Relation::Extended
end
