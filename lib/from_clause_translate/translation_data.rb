require 'from_clause_translate/fallback'

class FromClauseTranslate::TranslationData
  attr_reader :model, :translated, :plurals, :selections

  def initialize(model)
    @model = model
    @translated = {}
    @plurals = {}
    @selections = I18n.available_locales.map { |locale| [locale, {}] }.to_h
  end

  def translates?(column)
    @translated[column.to_sym] || false
  end

  def add_columns(columns)
    columns.each(&method(:add_column))
  end

  def add_column(column)
    hash = column.is_a?(Hash) ? column : {name: column}

    return if translates?(hash[:name]) ||
              !model.column_names.include?("#{hash[:name]}_#{I18n.locale}")

    column = hash[:name]
    translated[column] = true

    define_translated_selection column, hash

    column = column.to_s
    dashed = "#{column}_"
    define_translated_column_getter_and_setter column, dashed
    define_translated_column_changing_methods column, dashed
  end

  def add_plural(plural)
    plural = plural.to_sym
    translated[plural] = true
    column = plural.to_s.singularize.to_sym
    selection = I18n.available_locales.map do |locale|
      "#{model.quoted_table_name}.\"#{column}_#{locale}\""
    end.join ','
    plurals[plural] = selection
    I18n.available_locales.each do |locale|
      selections[locale][plural] = selection
    end
  end

  private

  def define_translated_selection(column, hash)
    fallback = FromClauseTranslate::Fallback.new(model, hash)
    I18n.available_locales.each do |locale|
      selection = "#{model.quoted_table_name}.\"#{column}_#{locale}\""
      selection = fallback.wrap selection, column, locale
      selections[locale][column] = "#{selection} AS #{column}"
    end
  end

  def define_translated_column_getter_and_setter col, dashed
    model.define_method col do
      @attributes.key?(col) ? self[col] : send("#{dashed}#{I18n.locale}")
    end

    model.define_method col + '=' do |val|
      send "#{dashed}#{I18n.locale}=", val
    end
  end

  def define_translated_column_changing_methods col, dashed
    model.define_method 'saved_change_to_' + col do
      send "saved_change_to_#{dashed}#{I18n.locale}"
    end

    %w[changed? before_last_save change_to_be_saved in_database].each do |suf|
      model.define_method dashed + suf do
        send "#{dashed}#{I18n.locale}_#{suf}"
      end
    end

    %w[saved_change_to will_save_change_to].each do |prefix|
      model.define_method "#{prefix}_#{col}?" do
        send "#{prefix}_#{dashed}#{I18n.locale}?"
      end
    end
  end
end
