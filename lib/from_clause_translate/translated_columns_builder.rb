class FromClauseTranslate::TranslatedColumnsBuilder
  def initialize relation, klass, without_projections
    @relation = relation
    @klass = klass
    @arel = relation.arel
    @without_projections = without_projections
    @columns = []
    @has_translated = false
  end

  def perform
    @skip_translated = []
    add_translated_columns_from_values
    add_translated_columns_from_projections
    add_translated_columns_from_cores
    add_translated_columns_from_joins
    add_translated_columns_from_orders

    @columns if @has_translated
  end

  private

  def add_translated_columns_from_values
    translated = @relation.instance_variable_get(:@values)[:translated]
    return unless translated

    @has_translated = true
    translated.each do |column|
      next translated_selection(column) unless column.is_a? Hash

      column.each do |key, value|
        @skip_translated << key.to_s
        @columns << "#{value} AS #{key}" unless value.nil?
      end
    end
  end

  def add_translated_columns_from_projections
    return if @without_projections

    @arel.projections.each do |column|
      next if column.is_a?(String) && column[0] != '"'

      arel_projection_translation column
    end
  end

  def arel_projection_translation column
    column = column.name if column.is_a? Arel::Attributes::Attribute
    column = column.to_s unless column.is_a? String

    name = column.first == '"' ? column[1...-1] : column

    return if @skip_translated.include? name

    translated_selection name, column
  end

  def add_translated_columns_from_cores
    @arel.ast.cores.each do |core|
      add_translated_columns_from_groups core
      add_translated_columns_from_nodes core.wheres
      add_translated_columns_from_nodes core.havings
    end
  end

  def add_translated_columns_from_groups core
    core.groups.each do |group|
      expr = group.expr
      name = expr[0] == '"' ? expr[1...-1] : expr
      translated_selection name
    end
  end

  def add_translated_columns_from_nodes nodes
    nodes.each do |node|
      node.children.each do |child|
        next unless child.respond_to? :left

        left = child.left
        if left.respond_to?(:relation) &&
           left.relation.table_name != @klass.table_name
          next
        end

        translated_selection child.left.name
      end
    end
  end

  def add_translated_columns_from_joins
    joins = @relation.instance_variable_get(:@values)[:joins]
    return unless joins

    joins.each do |join|
      ref = @klass.reflections[join.to_s]
      next if !ref || !ref.is_a?(ActiveRecord::Reflection::BelongsToReflection)

      translated_selection ref.join_keys.foreign_key
    end
  end

  def add_translated_columns_from_orders
    @arel.ast.orders.each do |name|
      if name.respond_to?(:expr)
        name = name.expr
        name = name.name if name.respond_to?(:name)
      end
      translated_selection name
    end
  end

  def translated_selection name, original = nil
    sym = name.to_sym
    translates = @klass.respond_to?(:translates?) && @klass.translates?(sym)
    @has_translated = true
    if translates
      return @columns << @klass._translation_data.selections[I18n.locale][sym]
    end

    return unless @klass.columns_hash[name.to_s]

    @columns << (original ? original.to_sym : sym)
  end
end
