module FromClauseTranslate
  require 'active_record'
  require 'from_clause_translate/active_record_relation'
  require 'from_clause_translate/class_methods'
  require 'from_clause_translate/translation_data'

  def self.included(model)
    model.extend FromClauseTranslate::ClassMethods
  end
end
