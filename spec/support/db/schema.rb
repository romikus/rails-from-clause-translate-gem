# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_02_155244) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "posts", force: :cascade do |t|
    t.string "slug_en"
    t.string "name_en"
    t.string "title_en"
    t.string "text_en"
    t.boolean "translated_en", default: false, null: false
    t.string "slug_ru"
    t.string "name_ru"
    t.string "title_ru"
    t.string "text_ru"
    t.boolean "translated_ru", default: false, null: false
    t.string "slug_uk"
    t.string "name_uk"
    t.string "title_uk"
    t.string "text_uk"
    t.boolean "translated_uk", default: false, null: false
    t.string "slug_de"
    t.string "name_de"
    t.string "title_de"
    t.string "text_de"
    t.boolean "translated_de", default: false, null: false
    t.string "slug_fr"
    t.string "name_fr"
    t.string "title_fr"
    t.string "text_fr"
    t.boolean "translated_fr", default: false, null: false
    t.string "slug_it"
    t.string "name_it"
    t.string "title_it"
    t.string "text_it"
    t.boolean "translated_it", default: false, null: false
    t.string "slug_es"
    t.string "name_es"
    t.string "title_es"
    t.string "text_es"
    t.boolean "translated_es", default: false, null: false
  end

end
