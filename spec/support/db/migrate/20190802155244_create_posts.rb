class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      %i[en ru uk de fr it es].each do |locale|
        t.string "slug_#{locale}"
        t.string "name_#{locale}"
        t.string "title_#{locale}"
        t.string "text_#{locale}"
        t.boolean "translated_#{locale}", null: false, default: false
      end
    end
  end
end
