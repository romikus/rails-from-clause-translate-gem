require 'spec_helper'

RSpec.describe 'Query methods' do
  let(:model) { Post }

  before :all do
    @post = Post.new
    (I18n.available_locales - [:de]).each do |locale|
      I18n.locale = locale
      @post.name = "Post name #{locale}"
      @post.title = "Post title #{locale}"
      @post.text = "Post text #{locale}"
      @post.slug = "post-name-#{locale}"
      @post.translated = true
    end
    @post.save!
  end

  before(:each) { I18n.locale = :en }

  attr_reader :post

  describe '.translated' do
    it 'selects translated values' do
      expect(model.translated(:name).take.name).to eq post.name
      expect(model.translated('name').take.name).to eq post.name

      record = model.translated(:slugs).take
      I18n.available_locales.each do |locale|
        I18n.locale = locale
        expect(record.slug).to eq post.send("slug_#{locale}")
      end
    end
  end

  describe '.group' do
    it 'adds group by statement and works' do
      expect(model.translated(:name).group(:name).to_sql).to include 'GROUP BY'
      expect(model.translated(:name).group(:name).take.name).to eq post.name
    end
  end

  describe '.having' do
    it 'adds having statement and works' do
      query = model.select(:name)
        .translated(:name, :translated)
        .group(:name).having('bool_and(translated) = true')

      expect(query.to_sql).to include 'HAVING'
      expect(query.take.name).to eq post.name
    end
  end

  describe '.order' do
    it 'orders collection by translated column' do
      expect(model.translated(:name).order(:name).take.name).to eq post.name
      expect(model.translated(:name).order('name DESC NULLS FIRST').take.name).to eq post.name
    end
  end

  describe '.where' do
    it 'filters using translated columns' do
      expect(model.translated(:name).where(translated: true).take.name).to eq post.name
      expect(model.translated(:name).where('translated' => true).take.name).to eq post.name
      expect(model.translated(:name, :translated).where('translated = true').take.name).to eq post.name
      expect{model.translated(:name).where('translated = true').take.name}.to(
        raise_error ActiveRecord::StatementInvalid
      )
    end
  end
end
