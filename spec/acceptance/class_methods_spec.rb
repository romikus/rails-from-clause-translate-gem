require 'spec_helper'

RSpec.describe 'Class methods' do
  let :model do
    model = Class.new(ActiveRecord::Base)
    model.table_name = 'posts'
    model.include FromClauseTranslate
    model.translates :name
    model
  end

  describe '.translates' do
    it 'should remember translatable columns' do
      expect(model.translates?(:text)).to be false
      model.translates :text
      expect(model.translates?(:text)).to be true

      arg = [:translated]
      expect(model).to receive(:translates_plurals).with(arg)
      model.translates plurals: arg
    end
  end

  describe '.translates_plurals' do
    it 'should remember translatable set of columns' do
      expect(model.translates?(:slugs)).to be false
      model.translates_plurals([:slugs])
      expect(model.translates?(:slugs)).to be true
    end
  end

  describe '.translates?' do
    it 'should say whether column is translated' do
      expect(model.translates?(:id)).to be false
      expect(model.translates?(:name)).to be true
    end
  end

  describe '.translated' do
    it 'allows building request with translated columns' do
      expect(model.translated(:name).is_a?(ActiveRecord::Relation)).to be true
    end
  end
end
