require 'spec_helper'

RSpec.describe 'Class methods' do
  let(:model) { Post }

  let(:name_en) { 'English name' }
  let(:new_name_en) { 'New english name' }
  let(:name_ru) { 'Russian name' }

  before { I18n.locale = :en }

  describe '#column' do
    it 'returns translated column value for current locale' do
      post = model.new(name_en: name_en, name_ru: name_ru)
      expect(post.name).to eq name_en
      I18n.locale = :ru
      expect(post.name).to eq name_ru
    end
  end

  describe '#column=' do
    it 'sets translated column value for current locale' do
      post = model.new(name_en: name_en, name_ru: name_ru)
      post.name = new_name_en
      expect(post.name).to eq new_name_en
      expect(post.name_en).to eq new_name_en
      expect(post.name_ru).to eq name_ru
    end
  end

  describe '#save_change_to_column' do
    it 'returns array with value before save and current value' do
      post = model.new(name: name_en)
      expect(post.saved_change_to_name).to be nil
      post.save!
      expect(post.saved_change_to_name).to eq [nil, name_en]
      post.update(name: new_name_en)
      expect(post.saved_change_to_name).to eq [name_en, new_name_en]
      expect(post.saved_change_to_name).to eq post.saved_change_to_name_en
    end
  end

  describe '#column_changed?' do
    it 'says whether column has not saved value' do
      post = model.new(name: name_en)
      expect(post.name_changed?).to be true
      post.save!
      expect(post.name_changed?).to be false
    end
  end

  describe '#column_before_last_save' do
    it 'has descriptive name' do
      post = model.new(name: name_en)
      expect(post.name_before_last_save).to be nil
      post.save!
      expect(post.name_before_last_save).to be nil
      post.update name: new_name_en
      expect(post.name_before_last_save).to eq name_en
    end
  end

  describe '#column_change_to_be_saved' do
    it 'gives array of saved value and new (not saved) value' do
      post = model.new(name: name_en)
      expect(post.name_change_to_be_saved).to eq [nil, name_en]
      post.save!
      post.name = new_name_en
      expect(post.name_change_to_be_saved).to eq [name_en, new_name_en]
    end
  end

  describe '#column_in_database' do
    it 'returns saved value' do
      post = model.new(name: name_en)
      expect(post.name_in_database).to be nil
      post.name = name_en
      expect(post.name_in_database).to be nil
      post.save!
      expect(post.name_in_database).to eq name_en
    end
  end

  describe '#saved_change_to_column?' do
    it 'says whether this column was changed in last save' do
      post = model.new(name: name_en)
      expect(post.saved_change_to_name?).to be false
      post.save!
      expect(post.saved_change_to_name?).to be true
    end
  end

  describe '#will_save_change_to_column?' do
    it 'gives true if column is going to be saved in next save' do
      post = model.new(name: name_en)
      expect(post.will_save_change_to_name?).to be true
      post.save!
      expect(post.will_save_change_to_name?).to be nil # activerecord bug, not mine =)
    end
  end
end