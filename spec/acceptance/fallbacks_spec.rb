require 'spec_helper'

RSpec.describe 'Fallbacks' do
  let(:model) { Post }

  before :all do

    I18n.locale = :uk
  end

  let(:name_uk) { 'Name uk' }
  let(:name_ru) { 'Name ru' }
  let(:name_en) { 'Name en' }

  describe 'fallback to another locale according to I18n.fallbacks' do
    attr_reader :post

    before do
      @post = Post.create(
        name_uk: name_uk,
        name_ru: name_ru,
        name_en: name_en
      )
    end

    it 'should retrieve ukrainian name' do
      expect(model.translated(:name).take.name).to eq post.name_uk
    end

    context 'ukrainian name is not set' do
      it 'should retrieve russian name' do
        post.update name_uk: nil
        expect(model.translated(:name).take.name).to eq post.name_ru
      end

      context 'russian name is not set' do
        it 'should retrieve english name' do
          post.update name_uk: nil, name_ru: nil
          expect(model.translated(:name).take.name).to eq post.name_en
        end
      end
    end
  end
end